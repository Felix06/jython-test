from java.lang import String
from java.util import HashMap

from lib import i_do_things, Haricot
from model.models import Person
from util import util

person = Person()
print person.name

print util.iam_useful(2, 4)

my_str = str(String())
my_str += "blabla"
print my_str

haricot = Haricot("dkad")
haricot.print_name()

i_do_things()

myMap = HashMap()
myMap.put("nounou", 10)
for k in myMap:
    print k
    print myMap.get(k)
# h = HelloWorld()
# h.hello()
# h.hello("TutorialsPoint")

print "Hello World !"